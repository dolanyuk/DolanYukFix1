package com.example.firda.dolanyukmenu;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import java.util.ArrayList;

/**
 * Created by Firda on 4/10/2018.
 */

public class Forum extends AppCompatActivity {

    AppBarLayout mBarLayout;
    Toolbar mToolbar;
    TabLayout mTabLayout;
    ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forum);

        //mengakses appbarlayout, toolbar, tablayout, viewpager pada layout
        mBarLayout = (AppBarLayout) findViewById(R.id.barlayout);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mTabLayout = (TabLayout) findViewById(R.id.tablayout);
        mViewPager = (ViewPager) findViewById(R.id.iniviewpager);

        //menentukan toolbar
        setSupportActionBar(mToolbar);
        setupVp(mViewPager);

        //mengikat tablayout dengan viewpager
        mTabLayout.setupWithViewPager(mViewPager);

        getIntent().getExtras();
    }

    //menentukan adapter untuk viewpager
    public void setupVp(ViewPager v) {
        VPAdapter adapter = new VPAdapter(getSupportFragmentManager());
        //set title fragment dengan nama berikut
        //set adapter untuk view
        v.setAdapter(adapter);
    }

    public void addPost(View view) {
        //berpindah ke postphoto class
        startActivity(new Intent(Forum.this, PostPhoto.class));
    }
    //subclass sebagai adapter untuk viewpager dan fragment
    class VPAdapter extends FragmentPagerAdapter {
        ArrayList<Fragment> listfragment;
        ArrayList<String>    listtitle;

        public VPAdapter(FragmentManager fm) {
            super(fm);
            listfragment  = new ArrayList<>();
            listtitle = new ArrayList<>();
        }

        @Override
        public Fragment getItem(int position) {
            return listfragment.get(position);
        }

        public void addFragment(Fragment f, String t){
            listfragment.add(f);
            listtitle.add(t);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return listtitle.get(position);
        }

        @Override
        public int getCount() {
            return listfragment.size();
        }
    }
}
