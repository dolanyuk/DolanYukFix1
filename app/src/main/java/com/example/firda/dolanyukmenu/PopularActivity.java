package com.example.firda.dolanyukmenu;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 4/10/2018.
 */

public class PopularActivity extends AppCompatActivity {
    List<PopularPlace> lstPopular ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_popular);
        getIntent().getExtras();

        lstPopular = new ArrayList<>();
        lstPopular.add(new PopularPlace("Pantai Pandawa",  "Popular Place","Pantai Pandawa adalah salah satu kawasan wisata di area Kuta selatan, Kabupaten Badung, Bali. Pantai ini terletak di balik perbukitan dan sering disebut sebagai Pantai Rahasia (Secret Beach). ",R.drawable.pandawa));
        lstPopular.add(new PopularPlace("Pantai Legian","Popular Place","Description Place",R.drawable.legian));
        lstPopular.add(new PopularPlace("Pantai Kuta","Popular Place","Description Place",R.drawable.kuta));
        lstPopular.add(new PopularPlace("Lafavela","Beach Club","Description Place",R.drawable.lavavela));
        lstPopular.add(new PopularPlace("Finns","Beach Club","Description Place",R.drawable.finns));
        lstPopular.add(new PopularPlace("Potato Head","Beach Club","Description Place",R.drawable.potato));
        lstPopular.add(new PopularPlace("Bali Zoo","Amusement Park","Description Place",R.drawable.balizoo));
        lstPopular.add(new PopularPlace("Kebun Raya Bali","Amusement Park","Description Place",R.drawable.kebunbali));
        lstPopular.add(new PopularPlace("Waterbom","Amusement Park","Description Place",R.drawable.waterbom));




        RecyclerView myrv = (RecyclerView) findViewById(R.id.recyclerview_id);
        RecycleviewAdapterPopular myAdapter = new RecycleviewAdapterPopular(this,lstPopular);
        myrv.setLayoutManager(new GridLayoutManager(this,3));
        myrv.setAdapter(myAdapter);


    }
}