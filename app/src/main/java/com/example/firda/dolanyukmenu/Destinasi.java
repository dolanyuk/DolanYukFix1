package com.example.firda.dolanyukmenu;

/**
 * Created by Firda on 3/7/2018.
 */

public class Destinasi {

    private String Judul;
    private String Kategori;
    private String Deskripsi;
    private int Foto;

    public Destinasi(){

    }

    public Destinasi(String judul, String kategori, String deskripsi, int foto){
        Judul = judul;
        Kategori = kategori;
        Deskripsi = deskripsi;
        Foto = foto;


    }

    public String getJudul(){
        return Judul;
    }
    public String getKategori(){
        return Kategori;
    }
    public String getDeskripsi(){
        return Deskripsi;
    }
    public int getFoto(){
        return Foto;
    }

    public void setJudul(String judul) {
        Judul = judul;
    }

    public void setKategori(String kategori) {
        Kategori = kategori;
    }

    public void setDeskripsi(String deskripsi) {
        Deskripsi = deskripsi;
    }

    public void setFoto(int foto) {
        Foto = foto;
    }
}
