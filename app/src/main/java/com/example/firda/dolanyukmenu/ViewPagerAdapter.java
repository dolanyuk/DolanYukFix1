package com.example.firda.dolanyukmenu;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ASUS on 4/10/2018.
 */

public class ViewPagerAdapter extends FragmentPagerAdapter {
    private final List<Fragment> stfragment = new ArrayList<>();
    private final List<String> Titles = new ArrayList<>();


    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return stfragment.get(position);
    }


    @Override
    public int getCount() {

        return Titles.size();
    }

    public CharSequence getPageTitle(int position){
        return Titles.get(position);
    }

    public void addFragment(Fragment fragment, String title) {
        stfragment.add(fragment);
        Titles.add(title);
    }
}

