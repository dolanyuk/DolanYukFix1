package com.example.firda.dolanyukmenu;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ASUS on 4/10/2018.
 */

public class fragmentContact extends HomeFragment {
    View v ;
    private RecyclerView myrecyclerview;
    private List<Contacts> contacts;


    public fragmentContact() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.contact_fragment, container, false);

        myrecyclerview = (RecyclerView) v.findViewById(R.id.contact_recycler);
        RecyclerViewAdapter recyclerAdapter = new RecyclerViewAdapter(getContext(), contacts);
        myrecyclerview.setLayoutManager(new LinearLayoutManager(getActivity()));
        myrecyclerview.setAdapter(recyclerAdapter);
        return v ;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        contacts = new ArrayList<>();
        contacts.add(new Contact("Miyabi", "(123) 4567890", R.drawable.winniethepooh));
        contacts.add(new Contact("Lucinta Luna", "(021) 73800437", R.drawable.lucintaluna));
        contacts.add(new Contact("Nikita Mirzani", "(0778) 6758320", R.drawable.nikitamirzani));
        contacts.add(new Contact("Ivan Gunawan", "(0119) 156389", R.drawable.ivangunawan));
        contacts.add(new Contact("Roro Fitria", "(0256) 934213", R.drawable.rorofitria));
        contacts.add(new Contact("Nassar", "(0864) 543914", R.drawable.nassar));
        contacts.add(new Contact("Fredich Yunadi", "(0123) 290382", R.drawable.fredichyunadi));
        contacts.add(new Contact("Dewi Perssik", "(0832) 5670123", R.drawable.dewipersik));
    }
}
