package com.example.firda.dolanyukmenu;

import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class DaftarHomestay extends AppCompatActivity {
    RecyclerView cd;
    menuAdapter adapter;
    List<pilihHomestay>listhomestay;
    private Configuration newConfig;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar_homestay);
        setTitle("List Homestay"); //set title untuk screennya

        //untuk meng INIT kan recyclerview dan adapternya//
        listhomestay = new ArrayList<>();
        cd = findViewById(R.id.recyclerView);
        cd.setHasFixedSize(true);

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            cd.setLayoutManager(new GridLayoutManager(DaftarHomestay.this, 2));
        } else {
            cd.setLayoutManager(new GridLayoutManager(DaftarHomestay.this, 1));
        }
        initdata();
    }



    private void initdata() {
        listhomestay.add(new pilihHomestay(R.drawable.home1, "Jono Homestay", "jono homestay terletak dikawasan legian bali. \n Beralamat di jln legian barat no 50 "));
        listhomestay.add(new pilihHomestay(R.drawable.home2, "Narti Homestay", "narti homestay terletak dikawasan kuta bali. \n Beralamat di jln kuta barat no 40"));
        listhomestay.add(new pilihHomestay(R.drawable.home3, "Nana Homestay", "nana homestay terletak dikawasan ubud bali. \n Beralamat di jln ubud barat no 30"));
        listhomestay.add(new pilihHomestay(R.drawable.home4, "Pras Homestay", "pras homestay terletak dikawasan denpasar bali. \n Beralamat di jln denpasar barat no 20"));

        adapter = new menuAdapter(this, listhomestay);
        cd.setAdapter(adapter);


    }
}
