package com.example.firda.dolanyukmenu;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class DetailHomestay extends AppCompatActivity {
    ImageView gambar;
    TextView nama, detail;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_homestay);
        setTitle("Detail Homestay");

        gambar = findViewById(R.id.gambar);
        nama = findViewById(R.id.detailNama);
        detail = findViewById(R.id.deskrip);

        gambar.setImageDrawable(this.getResources().getDrawable(Integer.valueOf(getIntent().getStringExtra("foto"))));
        nama.setText(getIntent().getStringExtra("nama"));

    }


    }

